/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  TouchableOpacity,
  Image,
  Linking,
  LogBox,
  YellowBox,
  ActivityIndicator,
  Dimensions,
} from 'react-native';

import {
  Container,
  CardItem,
  Text,
  Item,
  Input,
} from 'native-base';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { ConfirmDialog  } from 'react-native-simple-dialogs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import * as Animatable from 'react-native-animatable';

import AsyncStorage from '@react-native-community/async-storage';

LogBox.ignoreLogs([
  'VirtualizedLists should never be nested', // TODO: Remove when fixed
]);

const StackNavigation = createStackNavigator();
const TopTab = createMaterialTopTabNavigator();

const URL = 'http://116.203.181.21:8082/storage/'

class HomePage extends React.Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      openSettings: false,
      openModal: false
    }
  }

  
  async fetchData() {
    this.setState({
      loading: true,
      openModal: false
    })
    try {
      fetch('http://116.203.181.21:8082/jobs')
      .then((response) => response.json())
      .then((response) => {
        AsyncStorage.setItem('json', JSON.stringify(response))
        setTimeout(() => {
          this.setState({
            loading: false,
          });
        }, 1000);
      }).catch((err) => {
        console.log(err)
      });
    } catch (err) {
      alert('Une erreur est survenue, veuillez réessayez !')
    } 
  }

  render() {
      return (
        <>
          <Container style={{
            flex: 1,
            backgroundColor: '#ececec'
          }}>
            <SafeAreaView>
              <ScrollView >
            <View style={styles.imageContainer}>
              
              <TouchableOpacity style={{
                position:'absolute',
                width: 35,
                height: 35,
                right: 10,
                top: 30
              }}
                onPress={() => this.setState({
                  openSettings : !this.state.openSettings
                })}
              >
                <Image source={require('./images/settings.png')} style={{
                  width: 19,
                  height: 19
                }} />
              </TouchableOpacity>

                {this.state.openSettings == true ? (                  
                  <Animatable.View animation="fadeInRight"
                  duration={500}
                  style={{
                    position:'absolute',
                    right: 15,
                    top: 60,
                    width: 120,
                    height: 50,
                    backgroundColor: '#fff',
                    shadowColor: '#bfbfbf',
                    shadowOffset: {width: 2, height: 2},
                    shadowOpacity: 0.8,
                    shadowRadius: 2,
                    padding: 10
                  }}>
                    <TouchableOpacity>
                      <Text style={{
                        fontFamily:'Nexa-Light',
                        fontSize: 12,
                        color:'#000'
                      }}
                      onPress={()=>this.setState({
                        openModal: true
                      })}
                      >
                        Actualiser les données
                      </Text>
                    </TouchableOpacity>
                  </Animatable.View>
                ) : (
                  <View></View>
                )}

              <ConfirmDialog
                  style={{
                    fontFamily: 'Nexa-Regular'
                  }}
                  title="Mise à jour des données"
                  message="Une connexion internet est requise, êtes-vous sur ?"
                  visible={this.state.openModal}
                  onTouchOutside={() => this.setState({openModal: false})}
                  positiveButton={{
                      title: "Oui",
                      onPress: () => this.fetchData()
                  }}
                  negativeButton={{
                      title: "Non",
                      onPress: () => this.setState({openModal: false})
                  }}
              />

              <Animatable.Image animation="slideInUp" style={styles.images} source={require('./images/LOGO_Savemyjob-white.png')} />
              <View>
                <Text style={{
                  fontFamily:'Nexa-Bold',
                  color:'#fff',
                  fontSize: 15
                }}>Bienvenue</Text>
                <Text style={{
                  fontFamily: 'Nexa-Light',
                  color:'#fff',
                  fontSize: 15
                }}>sur SaveMyJob</Text>
              </View>
            </View>
            <View style={{
              backgroundColor:'#ececec',
              flex: 1,
              paddingLeft: 30,
              paddingTop: '10%',
              paddingRight: 30
            }}>
                <Text style={{
                  fontFamily: 'Nexa-Light',
                  fontSize: 15
                }}>
                  Grace à <Text style={{
                    fontFamily:'Nexa-Bold'
                  }}>SaveMyJob</Text>
                </Text>
                <Text style={{
                  fontFamily: 'Nexa-Light',
                  fontSize: 15
                }}>
                  consultez, découvrez et profitez des offres exceptionnelles proposées par les entreprises de Madagascar
                </Text>

                {this.state.loading == true ? (
                  <ActivityIndicator style={{
                    marginTop: 20,
                  }} color="#4ea8de" size="large"/>
                ): (
                  <TouchableOpacity style={{
                    backgroundColor:'#4ea8de',
                    marginTop: 20,
                    borderRadius: 20
                  }}
                    onPress={()=> this.props.navigation.navigate('JStack')}
                  >
                    <Text style={{
                      color:'#fff',
                      fontFamily:'Nexa-Bold',
                      textAlign:'center',
                      paddingTop: 10,
                      paddingBottom: 10,
                    }}>Découvrir</Text>
                  </TouchableOpacity>
                )}
            </View>
            
            
            </ScrollView>
            </SafeAreaView>
            <View style={{
              backgroundColor:'#ececec',
              textAlign:'center',
              flex: 1,
              maxHeight: 30,
              position:'absolute',
              bottom:0,
              width: Dimensions.get('window').width
            }}>
              <Text style={{
                textAlign:'center',
                marginBottom: 10,
                fontSize: 13,
                fontFamily:'Nexa-Light',
                paddingBottom: 15
              }}>Powered by UPENCY</Text>
            </View>
          </Container>
        </>
      );
    
  }
}

class Job extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      data: {
        categories: [],
      }
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  async fetchData() {
    try {
      let data = null;
      data = await AsyncStorage.getItem('json');
      if(data != null) {
        let DataParse = JSON.parse(data); 
        const truData = DataParse.jobs.filter((item) => {
          return item.id = this.props.route.params.id
        })
        this.setState({
          data: truData[0]
        })
      }
    } catch (err) {}
  }

  render() {
    
    const categories = this.state.data.categories.map((item) => {
      return item.label;
    });

    return (
      <>
        <View style={{
          flex: 1,
          justifyContent:'space-around',
          alignContent:'center',
          flexDirection:'row',
          alignItems:'center',
          maxHeight: 80,
          backgroundColor:'#ececec'
        }}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{
            width: 30,
            maxWidth: 30,
            position:'absolute',
            top: 30,
            left: 30,
          }}>
            <Image source={require('./images/left.png')} style={{
              width: 15,
              height: 15
            }}/>
          </TouchableOpacity>
          <Image style={{
            width: 170,
            height: 80
          }} source={require('./images/LOGO_Savemyjob_Paysage.png')} />
        </View>
        <View style={{
          flex: 1,
          backgroundColor: '#6930c3',
        }}>
        
            <Container padder style={{
              backgroundColor: '#6930c3',
              paddingLeft: 20,
              paddingRight: 20,
              maxHeight: Math.round(Dimensions.get('window').height / 2.6),
              paddingBottom: 20
            }}>
              {
                /*
                  <Image source={{
                    uri: `data:image/gif;base64,${this.state.data.logo}`
                  }}
                */
              }
              <Image source={{
                uri: URL + this.state.data.logo
              }}
              style={{
                width: 100,height: 100,
                backgroundColor:'#fff',
                shadowColor: '#bfbfbf',
                shadowOffset: {width: 2, height: 2},
                shadowOpacity: 0.8,
                shadowRadius: 2,
                marginTop: 30,
                marginBottom: 10
              }}
              />
              <Text style={{
                fontFamily:'Nexa-Regular',
                color:'#fff',
                fontSize: 20
              }}>{this.state.data.name}</Text>
              <Text style={{
                fontFamily:'Nexa-Light',
                color:'#fff',
                fontSize: 13,
                textTransform: 'capitalize'
              }}>{categories.join(' - ')}</Text>
              <Text style={{
                fontFamily:'Nexa-Regular',
                color:'#fff',
                fontSize: 12
              }}>{this.state.data.ville}</Text>
              <Text style={{
                fontFamily:'Nexa-Regular',
                color:'#fff',
                fontSize: 12
              }}>{this.state.data.adresse}</Text>
              
              <TouchableOpacity onPress={() => Linking.openURL(`tel:${this.state.data.phone}`)} style={{
                flex: 1,
                flexDirection: 'row',
                marginTop: 10
              }}>
                <Image source={require('./images/phone-call.png')} style={{
                  width: 15,
                  height: 15,
                  marginRight: 10,
                  marginTop: 5
                }}/>
                <Text style={{
                  fontFamily:'Nexa-Regular',
                  color:'#fff',
                  fontSize: 13,
                  marginTop: 5,
                }}>
                 {this.state.data.phone}</Text>
              </TouchableOpacity>
              
              <TouchableOpacity onPress={() => Linking.openURL(`mailto:${this.state.data.email}`)} style={{
                flex: 1,
                flexDirection: 'row',
                marginTop: 10
              }}>
                
                <Image source={require('./images/mail.png')} style={{
                  width: 15,
                  height: 15,
                  marginRight: 10
                }}/>
                <Text style={{
                  fontFamily:'Nexa-Regular',
                  color:'#fff',
                  fontSize: 13,
                }}>Email : {this.state.data.email}</Text>
              </TouchableOpacity>

            </Container>
          
            <SafeAreaView style={{
              flex: 1,
            }}>
              <ScrollView>  
                <View style={{
                  flex: 1,
                  backgroundColor:'#ececec',
                  paddingLeft: 15,
                  paddingRight: 15
                }}>
                  <View style={{
                    backgroundColor: '#ececec',
                    padding: 15
                  }}>
                    <Text style={{
                      fontFamily: 'Nexa-Bold',
                      color: '#000',
                      fontSize: 20,
                      marginTop: 20
                    }}>A propos de l'entreprise</Text>
                    <Text style={{
                      fontFamily: 'Nexa-Light',
                      fontSize: 15,
                      marginTop: 10
                    }}>{this.state.data.description}</Text>
                  </View>

                  <View style={{
                    backgroundColor: '#4ea8de',
                    padding: 15,
                    marginTop: 20
                  }}>
                    <Text style={{
                      fontFamily: 'Nexa',
                      color:'#fff',
                      fontSize: 15
                    }}>REMISE</Text>
                    <Text style={{
                      fontFamily: 'Nexa-Regular',
                      color:'#fff',
                      fontSize: 15,
                      marginTop: 10
                    }}>{this.state.data.remise}</Text>
                  </View>

                  <View style={{
                    backgroundColor: '#4ea8de',
                    padding: 15,
                    marginTop: 20,
                    marginBottom: 20
                  }}>
                    <Text style={{
                      fontFamily: 'Nexa-Regular',
                      color:'#fff',
                      fontSize: 15
                    }}>OFFRE</Text>
                    <Text style={{
                      fontFamily: 'Nexa-Regular',
                      color:'#fff',
                      fontSize: 15,
                      marginTop: 10
                    }}>{this.state.data.offre}</Text>
                  </View>

                </View>
              </ScrollView>
            </SafeAreaView>

        </View>

      </>
    );
  }
}

class TabChild extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      data: {
        all: [],
        jobs: [],
      }
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  async fetchData() {
    try {
      let data = null;
      data = await AsyncStorage.getItem('json');
      if(data != null) {
        let DataParse = JSON.parse(data);
        const currentName = this.props.route.name;
        let dParse = [];
        if (currentName == "tout") {
         
          this.setState({
            data: {
              jobs: DataParse.jobs
            }
          })
        } else if(currentName != "" && currentName != null) {
          dParse = DataParse.all.filter((item) => {
            return item.label == currentName
          })
          this.setState({
            data: {
              jobs : dParse[0].jobs
            }
          })
        } 
        
      }
    } catch (err) {
      console.log(err)
    }
  }


  render() {
    return (
      <>
        <Container padder style={{
          backgroundColor: '#ddd'
        }}>
          <SafeAreaView>
            <ScrollView>
              {this.state.data.jobs.map((data, index) => {
                const category = data.categories.map((item) => {
                  return item.label;
                }).map((item) => {
                  return item.toUpperCase()
                }).join(' - ');
                return (
                  <CardItem key={index} style={{
                    flex: 1,
                    flexDirection:'row',
                    alignContent:'center',
                    alignItems:'center',
                    borderBottomWidth: 1,
                    borderBottomColor: '#ddd',
                    paddingBottom: 20
                  }}>
                    <View style={{
                      marginRight: 20,
                      
                    }}>
                      {
                        /*
                          <Image source={{
                            uri: `data:image/gif;base64,${data.logo}`
                          }}
                        */
                      }
                      <Image source={{
                        uri: URL + data.logo
                      }}
                      style={{
                        width: 100,
                        height: 100,
                        shadowColor: '#bfbfbf',
                        shadowOffset: {width: 2, height: 2},
                        shadowOpacity: 0.8,
                        shadowRadius: 2
                      }}
                      />
                    </View>
                    <View style={{
                      maxWidth: '80%'
                    }}>
                    <Text style={{
                      fontFamily:'Nexa-Bold',
                      color:'#000',
                      fontSize: 15
                    }}>{data.name}</Text>
                    <Text style={{
                      fontFamily: 'Nexa-Light',
                      color:'#bfbfbf',
                      fontSize: 13
                    }}>{data.ville}</Text>
                    <Text style={{
                      fontFamily: 'Nexa-Light',
                      color:'#bfbfbf',
                      fontSize: 13
                    }}>{data.adresse}</Text>
                    <View style={{
                      flex: 1,
                      flexDirection:'row',
                      maxWidth: 300,
                      
                    }}>
                      <Text key={index} style={{
                          marginRight: 5,
                          fontFamily:'Nexa-Light',
                          color: '#63dfdd',
                          fontSize: 12,
                          textAlign:'left'
                        }}>{category}</Text>
                    </View>
                    <TouchableOpacity style={{
                      paddingLeft: 20,
                      paddingRight: 20,
                      paddingTop: 10,
                      paddingBottom: 10,
                      backgroundColor: '#4ea8de',
                      textAlign:'center',
                      borderRadius: 20,
                      marginTop: 10
                    }}
                    onPress={() => this.props.navigation.navigate('Job', {
                      id: data.id
                    })}
                    >
                      <Text style={{
                        color:'#fff',
                        fontSize: 14,
                        textAlign:'center',
                        fontFamily:'Nexa-Light'
                      }}>Voir l'offre</Text>
                    </TouchableOpacity>
                    </View>
                  </CardItem>
                );
              })}
            </ScrollView>
          </SafeAreaView>
        </Container>
      </>
    );
  }
}

class JobList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: {
        all: [],
        jobs: [],
      },
      categories: {},
      seek: false,
      seekValue: ""
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  async fetchData() {
    try {
      let data = null;
      data = await AsyncStorage.getItem('json');
      if(data != null) {
        let DataParse = JSON.parse(data);
      
        this.setState({
          data: DataParse,
        })
      }
    } catch (err) {}
  }

  renderNavigation() {

    const Tabs = this.state.data.all.map((item, index) => {
      return (<TopTab.Screen name={item.label} component={TabChild} key={index} />)
    })

    return (
      <TopTab.Navigator tabBarOptions={
        {
          activeTintColor:'#fff',
          scrollEnabled:true,
          indicatorStyle: {
            backgroundColor:'#fff'
          },
          style: {
            backgroundColor:'#6930c3'
          }
        }
      }>
        <TopTab.Screen name="tout" component={TabChild}/>
        {Tabs}
      </TopTab.Navigator>
    );
  }

  handleSeek(text) {
    this.setState({
      seek: true,
      seekValue: text
    })
  }

  closeSeek() {
    this.setState({
      seek: false,
      seekValue: ''
    })
  }

  renderChild() {
    if(this.state.seek && this.state.seekValue != "") {
      return (
        <Container padder style={{
          backgroundColor: '#ddd'
        }}>
          <SafeAreaView>
            <ScrollView>
              {this.state.data.jobs.filter(data => data.name.includes(this.state.seekValue)).map((data, index) => {
                const category = data.categories.map((item) => {
                  return item.label;
                }).map((item) => {
                  return item.toUpperCase()
                }).join(' - ');
                return (
                  <CardItem key={index} style={{
                    flex: 1,
                    flexDirection:'row',
                    alignContent:'center',
                    alignItems:'center',
                    borderBottomWidth: 1,
                    borderBottomColor: '#ddd',
                    paddingBottom: 20
                  }}>
                    <View style={{
                      marginRight: 20,
                      
                    }}>
                      {
                        /*
                          <Image source={{
                            uri: `data:image/gif;base64,${data.logo}`
                          }}
                        */
                      }
                      <Image source={{
                        uri: URL + data.logo
                      }}
                      style={{
                        width: 100,
                        height: 100,
                        shadowColor: '#bfbfbf',
                        shadowOffset: {width: 2, height: 2},
                        shadowOpacity: 0.8,
                        shadowRadius: 2
                      }}
                      />
                    </View>
                    <View style={{
                      maxWidth: '80%'
                    }}>
                    <Text style={{
                      fontFamily:'Nexa-Bold',
                      color:'#000',
                      fontSize: 15
                    }}>{data.name}</Text>
                    <Text style={{
                      fontFamily: 'Nexa-Light',
                      color:'#bfbfbf',
                      fontSize: 13
                    }}>{data.ville}</Text>
                    <Text style={{
                      fontFamily: 'Nexa-Light',
                      color:'#bfbfbf',
                      fontSize: 13
                    }}>{data.adresse}</Text>
                    <View style={{
                      flex: 1,
                      flexDirection:'row',
                      maxWidth: 300,
                      
                    }}>
                      <Text key={index} style={{
                          marginRight: 5,
                          fontFamily:'Nexa-Light',
                          color: '#63dfdd',
                          fontSize: 12,
                          textAlign:'left'
                        }}>{category}</Text>
                    </View>
                    <TouchableOpacity style={{
                      paddingLeft: 20,
                      paddingRight: 20,
                      paddingTop: 10,
                      paddingBottom: 10,
                      backgroundColor: '#4ea8de',
                      textAlign:'center',
                      borderRadius: 20,
                      marginTop: 10
                    }}
                    onPress={() => this.props.navigation.navigate('Job', {
                      id: data.id
                    })}
                    >
                      <Text style={{
                        color:'#fff',
                        fontSize: 14,
                        textAlign:'center'
                      }}>Voir l'offre</Text>
                    </TouchableOpacity>
                    </View>
                  </CardItem>
                );
              })}
            </ScrollView>
          </SafeAreaView>
        </Container>
      )
    } else {
      if(this.state.data.all.length > 0) {
        return (
          this.renderNavigation()
        )
      } else {
        (
          <ActivityIndicator size="large" />
        )
      }
    }
  }

  render() {


    return (
      <>
        <View style={{
          flex: 1,
          justifyContent:'space-around',
          alignContent:'center',
          flexDirection:'row',
          alignItems:'center',
          maxHeight: 80,
          backgroundColor:'#ececec'
        }}>
          <Image style={{
            width: 170,
            height: 80
          }} source={require('./images/LOGO_Savemyjob_Paysage.png')} />
        </View>
        <View>
          <Item regular>
            <Input placeholder='Rechercher' onChangeText={(text) => this.handleSeek(text)} value={this.state.seekValue} style={{
              fontFamily:'Nexa-Regular'
            }}/>
            {this.state.seek == true ? (
            <TouchableOpacity onPress={() => this.closeSeek()}>
              <Image  source={require('./images/close.png')} style={{
                width: 13,
                height: 13,
                marginRight: 15
              }}/>
            </TouchableOpacity>
            ) : (
              <View></View>
            )}
          </Item>
        </View>

        {this.renderChild()}

        
      </>
    );
  }
}

const JoBStack = createStackNavigator();

function JStack () {
  return (
    <JoBStack.Navigator
      initialRouteName="JobList"
      screenOptions={{
        headerShown: false,
      }}
    >
      <JoBStack.Screen name="JobList" component={JobList}/>
      <JoBStack.Screen name="Job" component={Job}/>
    </JoBStack.Navigator>
  );
} 

class ScreenStack extends React.Component {

  render() {
    return (
      <StackNavigation.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerShown: false,
        }}
      >
        <StackNavigation.Screen name="Home" component={HomePage} />
        <StackNavigation.Screen name="JStack" component={JStack} />
      </StackNavigation.Navigator>
    )
  }
}
class AppContainer extends React.Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    this.getJson();
  }

  async getJson() {
    try {
      let json = null;
      json = await AsyncStorage.getItem('json');
      if(json == null) {
        fetch('http://116.203.181.21:8082/jobs')
        .then((response) => response.json())
        .then((response) => {
          AsyncStorage.setItem('json', JSON.stringify(response))
          setTimeout(() => {
            this.setState({
              loading: false,
            });
          }, 2000);
        }).catch((err) => {
          console.log(err)
        });
      } else {
        setTimeout(() => {
          this.setState({
            loading: false,
          });
        }, 2000);
      }

    } catch (err) {

      const requestOptions = {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ err })
      };

      fetch('http://116.203.181.21:8080/logs',requestOptions);

      setTimeout(() => {
        this.setState({
          loading: false,
        });
      }, 1000);
    } 
  }

  render() {
    if (this.state.loading) {
      return (
        <>
          <View style={styles.imageContainer2}>
            <Animatable.Image
              animation="zoomIn"
              delay={500}
              style={styles.images}
              source={require('./images/LOGO_Savemyjob-white.png')} />
          </View>
        </>
      );
    } else {
      return (
        <NavigationContainer>
          <ScreenStack />
        </NavigationContainer>
      );
    }
  }
}

const styles = StyleSheet.create({
  card: {
    borderRadius: 10,
    padding: 0,
  },
  banner: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    backgroundColor:"#000",
    height: 140,  
  },
  bodyCard: {
    alignContent:'center',
    justifyContent:'center',
    textAlign: 'center',
    flex: 1,
    position:'relative',
    top: -80, 
  },
  logo: {
    height: 100,
    width: 100,
    borderRadius: 50,
    justifyContent: 'center',
    marginRight: 'auto',
    borderColor: '#fcfcfc66',
    borderWidth: 1,
    marginLeft: 'auto',
  },
  bouton: {
    backgroundColor: '#c1011e',
    marginRight: 'auto',
    marginLeft: 'auto',
    borderRadius: 50,
    marginTop: 30,
    marginBottom: -60,
  },
  boutonText: {
    fontFamily: 'Nexa',
    textAlign:'center',
    color:"#fff",
  },
  resto: {
    textAlign:'center',
    color: '#C12126',
    marginRight: 'auto',
    marginLeft: 'auto',
    fontFamily:'Nexa-Bold',
    fontSize: 20,
    marginBottom: 30,
  },
  address: {
    marginRight: 'auto',
    marginLeft: 'auto',
    fontFamily: 'Nexa-Light',
    textAlign:'center',
  },
  scanner: {
    height: "100%",
    backgroundColor: "#000"
  },
  flash: {
    justifyContent: "flex-end",
    alignItems: "flex-end",
    position: "absolute",
    zIndex: 999,
  },
  scrollView: {
    backgroundColor: "#000",
  },
  bgColor: {
    backgroundColor: "#68765c",
  },
  imageContainer: {
    backgroundColor: "#6930c3",
    //justifyContent: 'center',
    //alignItems: 'flex-start',
    textAlign: "left",
    //flex: 1,
    paddingLeft: 30,
    paddingBottom: '10%',
    paddingTop: '20%'
  },
  imageFooter: {
    backgroundColor: "#68765c",
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: "center",
    height: "8%",
    maxHeight: "8%"
  },
  imageContainer2: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    height: "100%",
    backgroundColor: "#6930c3",
    padding: 0,
    margin: 0
  },
  images: {
    width: Math.round(Dimensions.get('window').width/1.5),
    height: Math.round(Dimensions.get('window').height/3),
    alignContent:'flex-start',
    position:'relative',
    maxWidth: 220,
    maxHeight: 300
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: "#000",
    flex: 1
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777'
  },
  textBold: {
    fontWeight: '500',
    color: '#000'
  },
  buttonText: {
    fontSize: 13,
    color: '#fff',
    position: "relative",
    fontWeight: "200"
  },
  buttonTouchable: {
    padding: 16,
    flex: 1
  }
});

export default AppContainer;
